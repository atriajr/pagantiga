<!DOCTYPE html>

<html lang="en">

<head>

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-106457240-1', 'auto');
ga('send', 'pageview');

</script>

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

  <meta name="viewport" content="width=device-width, initial-scale=1"/>

  <title>Unitec Jr</title>



  <!-- CSS  -->

  <!--<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">-->

  <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>

  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>

  <link href="aos/aos.css" rel="stylesheet">

  <style type="text/css">
    /* fallback */
    @font-face {
      font-family: 'Material Icons';
      font-style: normal;
      font-weight: 400;
      src: url("fonts/Icons.woff2") format('woff2');
    }

    .material-icons {
      font-family: 'Material Icons';
      font-weight: normal;
      font-style: normal;
      font-size: 24px;
      line-height: 1;
      letter-spacing: normal;
      text-transform: none;
      display: inline-block;
      white-space: nowrap;
      word-wrap: normal;
      direction: ltr;
      -webkit-font-feature-settings: 'liga';
      -webkit-font-smoothing: antialiased;
    }
    </style>





</head>

<body>

  <?php require 'header.html' ?>

  <div class="container">

    <div class="section">

        <div class="row">

        <div class="col s12 m6">

          <div class="card">

            <div class="card-image">

              <img src="assets/engtelecom.jpg" height="228px">

            </div>

            <div class="card-content" style="text-align: center">

                <p>Telecomunicações</p>

            </div>

          </div>

        </div>

        <div class="col s12 m6">

          <div class="card">

            <div class="card-image">

              <img src="assets/engamb.jpg">

            </div>

            <div class="card-content" style="text-align: center">

                <p>Ambiental</p>

            </div>

          </div>

        </div>

      </div>



      <div class="row">

      P&D é uma sigla para 'Pesquisa e Desenvolvimento'. Nós da UNITEC estamos a todo momento buscando expandir nossa área de atuação, e capacitando membros nas áreas de Telecomunicações e Ambiental.

      </div>



      <div class="row">

      Se você possui ou faz parte de uma empresa que seja de uma dessas duas áreas, entre em contato com a gente! Nós teremos prazer de utilizar todo o conhecimento que adquirimos pela UNICAMP a favor de vocês!

      </div>



      <div class="row">

      Atualmente, estamos buscando aprender:

      </div>



      <div class="row" data-aos="fade-right" data-aos-duration="2000">



        <div class="col s12 m4">

          <div class="icon-block">

            <h2 class="center" style="color: #009688 !important"><i class="material-icons">event</i></h2>

            <h5 class="center">Auditoria Ambiental em Eventos</h5>

          </div>

        </div>



        <div class="col s12 m4">

          <div class="icon-block">

            <h2 class="center" style="color: #009688 !important"><i class="material-icons">opacity</i></h2>

            <h5 class="center">Tratamento de Agua e Resíduos</h5>

          </div>

        </div>



        <div class="col s12 m4">

          <div class="icon-block">

            <h2 class="center" style="color: #009688 !important"><i class="material-icons">filter_vintage</i></h2>

            <h5 class="center">Recuperação de Áreas Degradadas e Plantio Compensatório</h5>

          </div>

        </div>



      </div> 



      <div class="row" data-aos="fade-right" data-aos-duration="2000">



        <div class="col s12 m4">

          <div class="icon-block">

            <h2 class="center" style="color: #009688 !important"><i class="material-icons">filter_list</i></h2>

            <h5 class="center">Consultoria de Eficiência Energética e Hídrica</h5>

          </div>

        </div>



        <div class="col s12 m4">

          <div class="icon-block">

            <h2 class="center" style="color: #009688 !important"><i class="material-icons">grain</i></h2>

            <h5 class="center">Gestão de Resíduos Sólidos</h5>

          </div>

        </div>



        <div class="col s12 m4">

          <div class="icon-block">

            <h2 class="center" style="color: #009688 !important"><i class="material-icons">iso</i></h2>

            <h5 class="center">Planejamento em Instalações Elétricas</h5>

          </div>

        </div>



      </div>

    

      <div class="row" data-aos="fade-right" data-aos-duration="2000">



        <div class="col s12 m4">

          <div class="icon-block">

            <h2 class="center" style="color: #009688 !important"><i class="material-icons">network_check</i></h2>

            <h5 class="center">Automação Residencial e Sistemas Inteligentes</h5>

          </div>

        </div>



        <div class="col s12 m4">

          <div class="icon-block">

            <h2 class="center" style="color: #009688 !important"><i class="material-icons">rss_feed</i></h2>

            <h5 class="center">Soluções em RFID</h5>

          </div>

        </div>



        <div class="col s12 m4">

          <div class="icon-block">

            <h2 class="center" style="color: #009688 !important"><i class="material-icons">settings_ethernet</i></h2>

            <h5 class="center">Consultoria em Telecomunicações</h5>

          </div>

        </div>



      </div> 



      



    </div>

  </div>

  <?php require 'footer.html'; ?> 

  <!--  Scripts-->

  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>  

  <script src="js/materialize.min.js"></script>

  <script src="js/init.js"></script>

  <script src="aos/aos.js"></script>



  <script>

    AOS.init();

  </script>



  </body>

</html>

