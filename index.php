<!DOCTYPE html>
<html lang="br">
<head>

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-106457240-1', 'auto');
ga('send', 'pageview');

</script>

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>Unitec Jr</title>

  <!-- CSS  -->
  <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="aos/aos.css" rel="stylesheet"/>
  <!--<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/> -->

  <style type="text/css">
    /* fallback */
    @font-face {
      font-family: 'Material Icons';
      font-style: normal;
      font-weight: 400;
      src: url("fonts/Icons.woff2") format('woff2');
    }

    .material-icons {
      font-family: 'Material Icons';
      font-weight: normal;
      font-style: normal;
      font-size: 24px;
      line-height: 1;
      letter-spacing: normal;
      text-transform: none;
      display: inline-block;
      white-space: nowrap;
      word-wrap: normal;
      direction: ltr;
      -webkit-font-feature-settings: 'liga';
      -webkit-font-smoothing: antialiased;
    }
    </style>


</head>
<body>

  <?php require 'header.html' ?>
  
  <div id="index-banner" class="parallax-container" style="height: 550px">
    <div class="section no-pad-bot">
      <div class="container">
        <br><br>
        <h1 class="header center teal-text text-lighten-2" style="text-shadow: 2px 2px #000000; color:white !important;">Da melhor universidade do Brasil para você!</h1>
        <div class="row center">
          <h5 class="header col s12 light"  style="text-shadow: 1px 1px #000000; color:white !important;"> Websites, Aplicativos e Sistemas desenvolvidos por alunos da UNICAMP</h5>
        </div>
        <div class="row center">
          <a href="http://materializecss.com/getting-started.html" id="download-button" class="btn-large waves-effect waves-light teal lighten-1">Leia sobre nós</a>
        </div>
        <br><br>

      </div>
    </div>
    <div class="parallax"><img src="assets/background11.jpg" alt="Unsplashed background img 1"></div>
  </div>


  <div class="container">
    <div class="section">

      <div class="wrapper-button" style="text-align: center;top:50%;">
        <a href="servicos.html" id="download-button" class="btn-large waves-effect waves-light teal lighten-1">Nossos Serviços</a>
      </div>
        

      <div class="row" data-aos="fade-right" data-aos-duration="2000">

        <a href="servicos.html#index-banner-webdev" style="color: inherit">
        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center" style="color: #009688 !important"><i class="material-icons">web</i></h2>
            <h5 class="center">Sites Institucionais e Sistemas Online</h5>
          </div>
        </div>
        </a>

        <a href="servicos.html#index-banner-desktopdev" style="color: inherit">
        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center" style="color: #009688 !important"><i class="material-icons">desktop_windows</i></h2>
            <h5 class="center">Sistemas Desktop</h5>
          </div>
        </div>
        </a>

        <a href="servicos.html#index-banner-mobiledev" style="color: inherit">
        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center" style="color: #009688 !important"><i class="material-icons">smartphone</i></h2>
            <h5 class="center">Aplicativos Mobile</h5>
          </div>
        </div>
        </a>

      </div>
      
      <div class="row" data-aos="fade-right" data-aos-duration="2000">

        <div class="wrapper-button" style="text-align: center;top:50%;">
          <a id="download-button" style="cursor: pointer;" class="btn-large waves-effect waves-light teal lighten-1">Nossos Diferenciais</a>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center" style="color: #009688 !important"><i class="material-icons">highlight</i></h2>
            <h5 class="center">Conhecimento de Ponta</h5>
            <h6 class="center">Por estarmos na UNICAMP e em contato com o Mercado, conseguimos trazer o que há de melhor para os projetos</h6>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center" style="color: #009688 !important"><i class="material-icons">monetization_on</i></h2>
            <h5 class="center">Rentabilidade</h5>
            <h6 class="center">Como somos uma Empresa Junior, composta ainda por alunos, nosso preço por hora é muito menor se comparado com o do mercado. O que resulta em projetos bem mais baratos </h6>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center" style="color: #009688 !important"><i class="material-icons">chat_bubble_outline</i></h2>
            <h5 class="center">Comunicação</h5>
            <h6 class="center">Para uma melhor relação com o Cliente, estamos sempre mantendo um constante contato, para que ele possa opinar e acompanhar o desenvolvimento de seu projeto</h6>
          </div>
        </div>

      </div>  

    </div>
  </div>


  <div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
      <div class="container">
        <div class="row center">
          <h5 class="header col s12 light" style="text-shadow: 1px 1px #000000; color:white !important;">Nós da UNITEC somos profissionais em buscar o melhor!</h5>
        </div>
      </div>
    </div>
    <div class="parallax"><img src="assets/Projects.jpg" alt="Unsplashed background img 2"></div>
  </div>

  <div class="container">
    <div class="section">

      <div class="row" data-aos="fade-up" data-aos-duration="2000">
        <div class="col s12 center">
          <h3><i class="mdi-content-send brown-text"></i></h3>
          <h4>Parceiros</h4>
          
          <div class="col s12 m4">
          <div class="icon-block">
            <div class="card">
              <div class="card-image waves-effect waves-block waves-light" style="height: 150px">
                <a href="http://www.auctus.com.br" target="_blank"> <img class="activator" src="assets/parceiras/auctus_logo.png"> </a>
              </div>
              <div class="card-content">
                <span class="card-title activator grey-text text-darken-4">Auctus<i class="material-icons right">more_vert</i></span>
              </div>
              <div class="card-reveal">
                <span class="card-title grey-text text-darken-4">Sobre a Auctus<i class="material-icons right">close</i></span>
                <p>A Auctus é uma empresa “filha da Unicamp”. O objetivo de sua formação foi agregar profissionais ligados à Unicamp, reconhecidos como de alta capacidade técnica na área de treinamento e melhoria de processos, para auxiliar as empresas. Sobre a empresa: <br>
                <ul>
                  <li>Profissionais Graduados em faculdades de primeira linha e Doutores nas áreas de engenharia e TI;</li><br>
                  <li>Experiência prática na condução de projetos “Seis Sigma” e em gerenciamento de projetos (PMI®);</li><br>
                  <li>Especializados em formação de profissionais em empresas.</li>
                </ul>
                </p>
              </div>
            </div>
          </div>
          </div>
        
          <div class="col s12 m4">
          <div class="icon-block">
            <div class="card">
              <div class="card-image waves-effect waves-block waves-light" style="height: 150px">
                <a href="http://www.mixcopias.com.br" target="_blank"> <img class="activator" src="assets/parceiras/mix_logo.png"> </a>
              </div>
              <div class="card-content">
                <span class="card-title activator grey-text text-darken-4">Mix Gráfica<i class="material-icons right">more_vert</i></span>
              </div>
              <div class="card-reveal">
                <span class="card-title grey-text text-darken-4">Sobre a Mix Gráfica<i class="material-icons right">close</i></span>
                <p>A Mix é uma gráfica localizada no Centro de Limeira que atua realizando Consultas Seranas e SPCs, Banners, Adesivos e Folhetos, todos com excelente qualidade!</p>
              </div>
            </div>
          </div>
          </div>

          <div class="col s12 m4">
          <div class="icon-block">
            <div class="card">
              <div class="card-image waves-effect waves-block waves-light" style="background-color: black; height: 150px">
                <a href="http://conpec.com.br" target="_blank"> <img class="activator" src="assets/parceiras/conpec_logo.png"> </a>
              </div>
              <div class="card-content">
                <span class="card-title activator grey-text text-darken-4">Conpec<i class="material-icons right">more_vert</i></span>
              </div>
              <div class="card-reveal">
                <span class="card-title grey-text text-darken-4">Sobre a Conpec<i class="material-icons right">close</i></span>
                <p>A Conpec – Consultoria, Projetos e Estudos em Computação – é uma associação civil sem fins lucrativos que visa desenvolver soluções em computação para pessoas, empresas e para a sociedade, estabelecendo e ampliando os vínculos universidade-empresa e universidade-sociedade.</p>
              </div>
            </div>
          </div>
          </div>

        </div>
      </div>

      <div class="row" data-aos="fade-up" data-aos-duration="2000">
        <div class="col s12 center">
          
          <div class="col s12 m4">
          <div class="icon-block">
            <div class="card">
              <div class="card-image waves-effect waves-block waves-light" style="height: 150px">
                <a href="http://www.extecamp.unicamp.br" target="_blank"> <img class="activator" src="assets/parceiras/extecamplogo.png" height="121px" width="120px"> </a>
              </div>
              <div class="card-content">
                <span class="card-title activator grey-text text-darken-4">Extecamp<i class="material-icons right">more_vert</i></span>
              </div>
              <div class="card-reveal">
                <span class="card-title grey-text text-darken-4">Sobre a Extecamp<i class="material-icons right">close</i></span>
                <p>A Escola de Extensão da Unicamp - Extecamp, órgão da PREAC - Pró-Reitoria de Extensão e Assuntos Comunitários, tem o objetivo de administrar e estimular o oferecimento de cursos de extensão pela Unicamp, ampliando assim a efetividade da transferência de conhecimentos disponíveis na Universidade para a comunidade.</p>
              </div>
            </div>
          </div>
          </div>
        
          <div class="col s12 m4">
          <div class="icon-block">
            <div class="card">
              <div class="card-image waves-effect waves-block waves-light" style="height: 150px">
                <a href="https://www.facebook.com/sinergia.cte" target="_blank"> <img class="activator" src="assets/parceiras/sinergialogo.jpg"> </a>
              </div>
              <div class="card-content">
                <span class="card-title activator grey-text text-darken-4">Sinergia<i class="material-icons right">more_vert</i></span>
              </div>
              <div class="card-reveal">
                <span class="card-title grey-text text-darken-4">Sobre o Sinergia<i class="material-icons right">close</i></span>
                <p>Somos uma organização estudantil sem fins lucrativos, formada por estudantes da Faculdade de Ciências Aplicadas e de Tecnologia da Unicamp, apoiada por docentes, a qual possui foco em capacitar e desenvolver líderes para os diversos segmentos da cadeia de valor energética do Brasil através de um treinamento com duração de um semestre. Dentro da organização temos projetos em desenvolvimento com fins sociais, acadêmicos e de expansão do próprio Sinergia, a fim de alcançar cada vez mais pessoas sobre o tema energia e as áreas que tangem o setor.</p>
              </div>
            </div>
          </div>
          </div>

          <div class="col s12 m4">
          <div class="icon-block">
            <div class="card">
              <div class="card-image waves-effect waves-block waves-light" style="background-color: black; height: 150px">
                <a href="http://www.oricos.com.br" target="_blank"> <img class="activator" src="assets/parceiras/oricoslogo.png"> </a>
              </div>
              <div class="card-content">
                <span class="card-title activator grey-text text-darken-4">Oricos<i class="material-icons right">more_vert</i></span>
              </div>
              <div class="card-reveal">
                <span class="card-title grey-text text-darken-4">Sobre a ORICOS<i class="material-icons right">close</i></span>
                <p>A ORICOS realiza projetos para cuidar da Internet das empresas, com a garantia de controle, segurança, mobilidade das pessoas e disponibilidade da Internet!</p>
              </div>
            </div>
          </div>
          </div>

        </div>
      </div>

    </div>
  </div>


  <div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
      <div class="container">
        <div class="row center">
        </div>
      </div>
    </div>
    <div class="parallax"><img src="assets/sobre_time.jpg" alt="Unsplashed background img 3"></div>
  </div>

  <?php require 'footer.html'; ?> 



  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.min.js"></script>
  <script src="js/init.js"></script>
  <script src="aos/aos.js"></script>

  <script>
    AOS.init();
  </script>

    <script>
      $('#download-button').click(function(event){
        event.preventDefault();
      });
    </script>

    
  </body>
</html>
