<!DOCTYPE html>

<html lang="en">

<head>

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-106457240-1', 'auto');
ga('send', 'pageview');

</script>

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

  <meta name="viewport" content="width=device-width, initial-scale=1"/>

  <title>Unitec Jr</title>



  <!-- CSS  -->

  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <link href="../css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>

  <link href="../css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>

  <link href="../css/blog.css" type="text/css" rel="stylesheet"/>

  <link href="../aos/aos.css" rel="stylesheet">





</head>

<body>

  <?php require '../header.html' ?>

  <div class="row">

  <div class="my-website-blog-area">Blog goes here.</div>

  </div>


  
  
  <?php require '../footer.html'; ?> 

  <!--  Scripts-->

  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

  <script src="../js/materialize.min.js"></script>

  <script src="../js/init.js"></script>

  <script src="../aos/aos.js"></script>

  <script>

    AOS.init();

  </script>
  
  </body>

</html>

