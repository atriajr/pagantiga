<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>

<html lang="en">

<head>

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-106457240-1', 'auto');
ga('send', 'pageview');

</script>

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

  <meta name="viewport" content="width=device-width, initial-scale=1"/>

  <title>Unitec Jr</title>



  <!-- CSS  -->

  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <link href="../css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="../../../../../css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>


  <link href="../css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="../../../../../css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  	
  <link href="../aos/aos.css" rel="stylesheet"/>
  <link href="../../../../../aos/aos.css" rel="stylesheet"/> 

  <link href="../../../../../css/blog.css" type="text/css" rel="stylesheet"/>

</head>

<body <?php body_class(); ?>>

		<div id="page" class="site">
		<?php require 'blogheader.html' ?>

		<div class="parallax-container valign-wrapper" style="height: 350px">
		<div class="section no-pad-bot">
		  <div class="container">
			<div class="row center">
			</div>
		  </div>
		</div>
		<div class="parallax"><img src="../../../../../assets/blogbanner.png" alt="Unsplashed background img 3"></div>
	  </div>

		<?php if ( has_nav_menu( 'top' ) ) : ?>
			<div class="navigation-top">
				<div class="wrap">
					<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
				</div><!-- .wrap -->
			</div><!-- .navigation-top -->
		<?php endif; ?>

	</header><!-- #masthead -->

	<?php

	/*
	 * If a regular post or page, and not the front page, show the featured image.
	 * Using get_queried_object_id() here since the $post global may not be set before a call to the_post().
	 */
	if ( ( is_single() || ( is_page() && ! twentyseventeen_is_frontpage() ) ) && has_post_thumbnail( get_queried_object_id() ) ) :
		echo '<div class="single-featured-image-header">';
		echo get_the_post_thumbnail( get_queried_object_id(), 'twentyseventeen-featured-image' );
		echo '</div><!-- .single-featured-image-header -->';
	endif;
	?>

	<!--  Scripts-->

	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

	<script src="../js/materialize.min.js"></script>

	<script src="../js/init.js"></script>

	<div class="site-content-contain">
		<div id="content" class="site-content">
