<!DOCTYPE html>

<html lang="en">

<head>

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-106457240-1', 'auto');
ga('send', 'pageview');

</script>

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

  <meta name="viewport" content="width=device-width, initial-scale=1"/>

  <title>Unitec Jr</title>



  <!-- CSS  -->

  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>

  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>

  <link href="aos/aos.css" rel="stylesheet">





</head>

<body>

  <?php require 'header.html' ?>

  <!-- Modal Structure -->

  <div id="modal1" class="modal">

    <div class="modal-content">

      <h4 id="tituloModal"></h4>

      <p id="conteudoModal"></p>

    </div>

    <div class="modal-footer">

      <a href="contato.html" class="modal-action modal-close waves-effect waves-green btn-flat">Interessado? Entre em contato com a gente!</a>

    </div>

  </div>





  <div id="index-banner-webdev" class="parallax-container">

    <div class="section no-pad-bot">

      <div class="container">

        <br><br>

        <h1 class="header center teal-text text-lighten-2" style="text-shadow: 2px 2px #000000; color:white !important;">Sites Institucionais e Sistemas Online</h1>

        <br><br>

      </div>

    </div>

    <div class="parallax"><img src="assets/webdev.jpg" alt="Unsplashed background img 1"></div>

  </div>





  <div class="container">

    <div class="section">



      Sites Institucionais e Sistemas Online são aqueles que você pode acessar digitando o endereço em seu navegador. Dentre suas diversas vantagens, podemos levantar as seguntes:



    </div>



    <div class="row" style="text-align: center;top:50%;">

      

        <a id="download-button" class="btn-large waves-effect waves-light teal lighten-1 modal-trigger exclusividade" onclick="preencherModal('exclusividade')" href="#modal1">Exclusividade</a>

          

        <a id="download-button" class="btn-large waves-effect waves-light teal lighten-1 modal-trigger divulgacao" onclick="preencherModal('divulgacao')" href="#modal1">Divulgação Barata e Eficaz</a>

      

        <a id="download-button" class="btn-large waves-effect waves-light teal lighten-1 modal-trigger resultados" onclick="preencherModal('mensuracao')" href="#modal1">Mensuração de Resultados</a>

    </div>







  </div>



  <div id="index-banner-desktopdev" class="parallax-container">

    <div class="section no-pad-bot">

      <div class="container">

        <br><br>

        <h1 class="header center teal-text text-lighten-2" style="text-shadow: 2px 2px #000000; color:white !important;">Sistemas Desktop</h1>

        <br><br>

      </div>

    </div>

    <div class="parallax"><img src="assets/desktopdev.jpg" alt="Unsplashed background img 1"></div>

  </div>





  <div class="container">

    <div class="section">

    

    Sistemas Desktop são aqueles em que você instala em seus próprios computadores e os executa com um clique duplo. Dentre suas diversas vantagens, podemos levantar as seguntes:

      

    </div>

    

    <div class="row" style="text-align: center;top:50%;">

        

        <a id="download-button" class="btn-large waves-effect waves-light teal lighten-1 modal-trigger" href="#modal1" onclick="preencherModal('exclusividade')">Exclusividade</a>

          

        <a id="download-button" class="btn-large waves-effect waves-light teal lighten-1 modal-trigger" href="#modal1" onclick="preencherModal('desempenho')">Desempenho</a>

      

        <a id="download-button" class="btn-large waves-effect waves-light teal lighten-1 modal-trigger" href="#modal1" onclick="preencherModal('conforto')">Conforto</a>

    </div>



    

  </div>



  <div id="index-banner-mobiledev" class="parallax-container">

    <div class="section no-pad-bot">

      <div class="container">

        <br><br>

        <h1 class="header center teal-text text-lighten-2" style="text-shadow: 2px 2px #000000; color:white !important;">Aplicativos Mobile</h1>

        <br><br>

      </div>

    </div>

    <div class="parallax"><img src="assets/mobiledev.png" alt="Unsplashed background img 1"></div>

  </div>





  <div class="container">

    <div class="section">



        Aplicativos Mobile são os aplicativos que você possui em seus celulares. Dentre suas diversas vantagens, podemos levantar as seguntes:



    </div>



        <div class="row" style="text-align: center;top:50%;">

      

        <a id="download-button" class="btn-large waves-effect waves-light teal lighten-1 modal-trigger" href="#modal1" onclick="preencherModal('exclusividade')">Exclusividade</a>

          

        <a id="download-button" class="btn-large waves-effect waves-light teal lighten-1 modal-trigger" href="#modal1" onclick="preencherModal('mobilidade')">Mobilidade</a>

      

        <a id="download-button" class="btn-large waves-effect waves-light teal lighten-1 modal-trigger" href="#modal1" onclick="preencherModal('experiencia')">Excelente Experiência do Usuário</a>

        </div>

    

  </div>

  <?php require 'footer.html'; ?> 

  <!--  Scripts-->

  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>  

  <script src="js/materialize.min.js"></script>

  <script src="js/init.js"></script>

  <script src="aos/aos.js"></script>



  <script>

      $(document).ready(function(){

        $('.modal').modal();

      });



      function preencherModal(vantagem){

          if(vantagem == "divulgacao"){

            $("#tituloModal").text("Por que um Site ou Sistema Web me fornece uma Divulgação Barata e Eficaz?");

            $("#conteudoModal").text("O Investimento em um Site ou Sistema Web é uma forma de divulgação contínua para todos os seus clientes enquanto ele estiver online. Uma vez que ele esteja pronto, seus clientes poderão encontrá-lo em ferramentas de busca como o Google e fazer seu uso, sem que você precise fazer um investimento adicional.");

          }



          if(vantagem == "exclusividade"){

            $("#tituloModal").text("Por que um Site ou Sistema Web me fornece Exclusividade?");

            $("#conteudoModal").text("A UNITEC pode fazer para você um site ou sistema web que contenha exatamente o que você pedir. O fato de ele ser exatamente conforme você requisitar, faz dele também único, e exclusivo para você! ");

          }



          if(vantagem == "mensuracao"){

            $("#tituloModal").text("Por que um Site ou Sistema Web me fornece Mensuração dos Resultados?");

            $("#conteudoModal").text("Além de você poder analisar o número de clientes que vão procurar por você depois de estar com o site online, o Google possui ferramentas como o Analytics que conseguem lhe dar dados reais de quantas pessoas estão visualzando seu site e para onde estão indo.");

          }



          if(vantagem == "desempenho"){

            $("#tituloModal").text("Por que um Sistema Desktop me fornece Desempenho?");

            $("#conteudoModal").text("Graças ao fato do Sistema estar instalado em seu computador, ele pode fazer uso de seus recursos sem que seu processamento não dependa integralmente de um servidor, o que o torna mais rápido e poderoso!");

          }



          if(vantagem == "conforto"){

            $("#tituloModal").text("Por que um Sistema Desktop me fornece Conforto?");

            $("#conteudoModal").text("Ter o próprio Sistema instalado no próprio computador significa acima de tudo praticidade. Utilizar do que você precisa, com um clique duplo sobre ele em sua Área de Trabalho!");

          }



          if(vantagem == "mobilidade"){

            $("#tituloModal").text("Por que um Sistema Desktop me fornece Mobilidade?");

            $("#conteudoModal").text("Como os aplicativos são instalados em nossos celulares e nós andamos com eles para todos os lados, podemos utilizar dos aplicativos onde quer que estejamos!");

          }



          if(vantagem == "experiencia"){

            $("#tituloModal").text("Por que um Sistema Desktop me fornece uma Excelente Experiência do Usuário?");

            $("#conteudoModal").text("Como hoje estamos todos utilizando celulares, nada melhor do que termos o que precisamos em nossos celulares!");

          }



      }

      </script>



  <script>

    AOS.init();

  </script>



  </body>

</html>

