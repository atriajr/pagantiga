<!DOCTYPE html>

<html lang="en">

<head>

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-106457240-1', 'auto');
ga('send', 'pageview');

</script>

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

  <meta name="viewport" content="width=device-width, initial-scale=1"/>

  <title>Unitec Jr</title>

  



  <!-- CSS  -->

  <!--<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">-->

  <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>

  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>

  <link href="aos/aos.css" rel="stylesheet">

  <style type="text/css">
    /* fallback */
    @font-face {
      font-family: 'Material Icons';
      font-style: normal;
      font-weight: 400;
      src: url("fonts/Icons.woff2") format('woff2');
    }

    .material-icons {
      font-family: 'Material Icons';
      font-weight: normal;
      font-style: normal;
      font-size: 24px;
      line-height: 1;
      letter-spacing: normal;
      text-transform: none;
      display: inline-block;
      white-space: nowrap;
      word-wrap: normal;
      direction: ltr;
      -webkit-font-feature-settings: 'liga';
      -webkit-font-smoothing: antialiased;
    }
    </style>





</head>

<body>

<?php require 'header.html'; ?>

  <div class="container">

    <div class="section">

      <div class="row">

        <div class="col s12 m6">

          <div class="card">

            <div class="card-image">

              <img src="assets/sobre_time2.jpg">

              <span class="card-title">Quem somos nós</span>

            </div>

            <div class="card-action">

              A Unitec é uma Empresa Junior da Faculdade de Tecnologia da UNICAMP. Como Empresa Junior, nossa principal característica é a que somos uma Empresa formada por estudantes ainda na graduação que buscam conhecer mais o mercado, entrar em contato com empresas maiores e se desenvolver profissionalmente e tecnicamente!

            </div>

          </div>

        </div>

        <div class="col s12 m6">

          <div class="card">

            <div class="card-image">

              <img src="assets/mej.png">

              <span class="card-title">Movimento Empresa Junior</span>

            </div>

            <div class="card-action">

              O MEJ (Movimento Empresa Junior) tem como propósito a busca por um Brasil Empreendedor. Trabalhamos todos os dias para formar pessoas comprometidas e capazes de transformar o Brasil por meio da realização de mais e melhores projetos. O movimento surgiu em 1967 na França e veio para o Brasil em 1988.

            </div>

          </div>

        </div>

      </div>



    </div>

  </div>

  <?php require 'footer.html'; ?>

  <!--  Scripts-->

  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

  <script src="js/materialize.min.js"></script>

  <script src="js/init.js"></script>

  <script src="aos/aos.js"></script>



  <script>

    AOS.init();

  </script>



  </body>

</html>

