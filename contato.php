<!DOCTYPE html>

<html lang="en">

<head>

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-106457240-1', 'auto');
ga('send', 'pageview');

</script>

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

  <meta name="viewport" content="width=device-width, initial-scale=1"/>

  <title>Unitec Jr</title>



  <!-- CSS  -->

  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>

  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>

  <link href="aos/aos.css" rel="stylesheet">





</head>

<body>

  <?php require 'header.html' ?>

  <div class="row">

  <div id="index-banner" class="parallax-container">

    <div class="section no-pad-bot">

      <div class="container">

        <br><br>

        <br><br>

      </div>

    </div>

    <div class="parallax"><img src="assets/frontunitec.jpeg" alt="Unsplashed background img 1"></div>

  </div>





  </div>



  <div class="row">

        <div class="col s12 m4">

          <div class="icon-block">

            <h2 class="center" style="color: #009688 !important"><i class="material-icons">phone</i></h2>

            <h5 class="center">Telefone</h5>

            <h6 class="center">+55 19 2113 3360</h6>

          </div>

        </div>



        <div class="col s12 m4">

          <div class="icon-block">

            <h2 class="center" style="color: #009688 !important"><i class="material-icons">email</i></h2>

            <h5 class="center">Email</h5>

            <h6 class="center">unitec@ft.unicamp.br</h6>

          </div>

        </div>



        <div class="col s12 m4">

          <div class="icon-block">

            <h2 class="center" style="color: #009688 !important"><i class="material-icons">location_on</i></h2>

            <h5 class="center">Endereço</h5>

            <h6 class="center">Rua Paschoal Marmo, 1888 - CEP: 13484-332. Jardim São Paulo - Limeira, SP. No Interior da Faculdade de Tecnologia.</h6>

          </div>

        </div>



      </div>    

      <?php require 'footer.html'; ?> 

  <!--  Scripts-->

  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

  <script src="js/materialize.min.js"></script>

  <script src="js/init.js"></script>

  <script src="aos/aos.js"></script>



  <script>

    AOS.init();

  </script>



  </body>

</html>

